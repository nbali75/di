package week2.nbali

import org.junit.Before
import org.junit.Test
import week2.nbali.data.*
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class MeetSantaGlobal {

    // Inject from global Registry.DEFAULT
    private val santa: Santa = inject()

    fun meet(children: List<Child>) {
        children.forEach { child ->
            val present = santa.choosePresentFor(child)
            present?.let {
                santa.bag.remove(present)
                child.present = present
            }
        }
    }
}

class RegistryGlobalTest {

    companion object {
        init {
            normal {
                listOf("bag of charcoal", "dog", "dog", "cat").asBag()
            }

            normal {
                listOf("Bob").asNaughtyChildList()
            }

            singleton<Santa> { SantaImpl(inject(), inject()) }
            //singleton { SantaImpl(inject(), inject()) as Santa}
        }
    }

    private lateinit var meetSanta: MeetSantaGlobal

    @Before
    fun setup() {
        meetSanta = MeetSantaGlobal()
    }

    @Test
    fun goodBoysGetPresent() {
        val peter = Child("Peter", listOf("dog"))
        meetSanta.meet(listOf(peter))
        assertEquals("dog", peter.present)
    }

    @Test
    fun naughtyBoysGetCharcoal() {
        val bob = Child("Bob", listOf("dog"))
        meetSanta.meet(listOf(bob))
        assertEquals("bag of charcoal", bob.present)
    }

    @Test
    fun goodBoysWithExpensiveWishGetPresent() {
        val peter = Child("Peter", listOf("nokia 8"))
        meetSanta.meet(listOf(peter))
        assertTrue(listOf("cat", "dog").contains(peter.present))
    }
}