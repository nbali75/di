package week2.nbali

import org.junit.Before
import org.junit.Test
import week2.nbali.data.Bag
import week2.nbali.data.NaughtyChildList
import week2.nbali.data.Santa
import week2.nbali.data.SantaImpl
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class RegistryTest {

    lateinit var registry: Registry
    @Before
    fun setup() {
        registry = Registry()
    }

    @Test
    fun whenRegisterSingletonClass_shouldInjectReturnIt() {
        registry.singleton { Bag() }

        val bag: Bag = registry.inject()
        assertNotNull(bag)
    }


    @Test
    fun whenUseInjectInSingleton_shouldInjectIt() {
        registry.normal { Bag() }
        registry.normal { NaughtyChildList() }
        registry.singleton { SantaImpl(inject(), inject()) as Santa }

        val p: Santa = registry.inject()
        assertNotNull(p)
    }

    @Test
    fun whenInjectMoreSingleton_shouldBeTheSame() {
        registry.singleton { Bag() }

        val bag1: Bag = registry.inject()
        val bag2: Bag = registry.inject()
        assertTrue(bag1 === bag2)
    }

    @Test
    fun whenInjectMoreNormal_shouldntBeTheSame() {
        registry.normal { Bag() }

        val bag1: Bag = registry.inject()
        val bag2: Bag = registry.inject()
        assertTrue(bag1 !== bag2)
    }

    @Test(expected = RegistryException::class)
    fun addSameNormalTwice_shouldThrowException() {
        registry.normal { Bag() }
        registry.normal { Bag() }
    }

    @Test(expected = RegistryException::class)
    fun addSameSignletonTwice_shouldThrowException() {
        registry.singleton { Bag() }
        registry.singleton { Bag() }
    }

    @Test(expected = RegistryException::class)
    fun addSameNormalAfterSingleton_shouldThrowException() {
        registry.singleton { Bag() }
        registry.normal { Bag() }
    }

    @Test(expected = RegistryException::class)
    fun addSameSingletonAfterNormal_shouldThrowException() {
        registry.normal { Bag() }
        registry.singleton { Bag() }
    }

    @Test(expected = RegistryException::class)
    fun whenClassNotExistsInRegistry_shouldThrowException() {
        val s : String = registry.inject()
    }
}