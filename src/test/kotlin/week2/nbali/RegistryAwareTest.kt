package week2.nbali

import org.junit.Before
import org.junit.Test
import week2.nbali.data.*
import kotlin.test.assertEquals
import kotlin.test.assertTrue


class MeetSantaRegistryAware(override val registry: Registry) : RegistryAware {

    // Inject from RegistryAware.registry
    private val santa: Santa = inject()

    fun meet(children: List<Child>) {
        children.forEach { child ->
            val present = santa.choosePresentFor(child)
            present?.let {
                santa.bag.remove(present)
                child.present = present
            }
        }
    }
}

class RegistryAwareTest {

    private lateinit var meetSanta: MeetSantaRegistryAware

    @Before
    fun setup() {
        val registry = Registry {
            normal {
                listOf("bag of charcoal", "dog", "cat").asBag()
            }

            normal {
                listOf("Bob").asNaughtyChildList()
            }

            singleton<Santa> {
                SantaImpl(inject(), inject())
            }
        }

        meetSanta = MeetSantaRegistryAware(registry)
    }

    @Test
    fun goodBoysGetPresent() {
        val peter = Child("Peter", listOf("dog"))
        meetSanta.meet(listOf(peter))
        assertEquals("dog", peter.present)
    }

    @Test
    fun naughtyBoysGetCharcoal() {
        val bob = Child("Bob", listOf("dog"))
        meetSanta.meet(listOf(bob))
        assertEquals("bag of charcoal", bob.present)
    }

    @Test
    fun goodBoysWithExpensiveWishGetPresent() {
        val peter = Child("Peter", listOf("nokia 8"))
        meetSanta.meet(listOf(peter))
        assertTrue(listOf("cat", "dog").contains(peter.present))
    }
}