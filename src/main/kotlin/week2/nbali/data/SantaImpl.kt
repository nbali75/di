package week2.nbali.data

class SantaImpl(override val bag: Bag, private val naughtyChildList: NaughtyChildList) : Santa {
    override fun choosePresentFor(child: Child): String? {
        if (naughtyChildList.contains(child.name)) {
            return bag.firstOrNull { it == "bag of charcoal" }
        }

        return bag.firstOrNull { child.wishList.contains(it) } ?: bag.firstOrNull { it != "bag of charcoal" }
    }
}