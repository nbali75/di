package week2.nbali.data

class Bag() {

    constructor(presents: List<String>) : this() {
        this.presents.addAll(presents)
    }

    private val presents = mutableListOf<String>()

    fun add(present: String) = presents.add(present)

    fun firstOrNull(filter: (String) -> Boolean) = presents.firstOrNull(filter)

    fun remove(present: String) = presents.remove(present)

    fun contains(present: String) = presents.contains(present)
}

fun List<String>.asBag() = Bag(this)