package week2.nbali.data

interface Santa {
    val bag: Bag

    fun choosePresentFor(child: Child): String?
}