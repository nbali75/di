package week2.nbali.data

class NaughtyChildList(names: List<String> = emptyList()) : ArrayList<String>(names)


fun List<String>.asNaughtyChildList() = NaughtyChildList(this)
