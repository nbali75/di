package week2.nbali

interface RegistryAware{
    val registry : Registry
}

inline fun <reified T>RegistryAware.inject() : T {
    return registry.inject()
}