package week2.nbali

import kotlin.reflect.KClass

class Registry {
    companion object {
        val DEFAULT = Registry()
    }

    constructor(init: Registry.() -> Unit = {}) {
        init.invoke(this)
    }

    @PublishedApi
    internal val creatorsRegistry: MutableMap<String, () -> Any?> = mutableMapOf()

    @PublishedApi
    internal val instances: MutableMap<String, Any?> = mutableMapOf()

    inline fun <reified T> singleton(noinline creator: Registry.() -> T) {
        val key = T::class.asKey()

        if (exists(key)) {
            throw RegistryException("The $key already exists in registry.")
        }

        creatorsRegistry.put(key, { instances.getOrPut(key, { creator.invoke(this) }) as T })
    }

    inline fun <reified T> normal(noinline creator: Registry.() -> T) {
        val key = T::class.asKey()

        if (exists(key)) {
            throw RegistryException("The $key already exists in registry.")
        }

        creatorsRegistry.put(key, { creator(this) })
    }

    inline fun <reified T> inject(): T {
        val key = T::class.asKey()
        if (!exists(key)) {
            throw RegistryException("The $key not exists in registry.")
        }
        return creatorsRegistry[key]?.invoke() as T
    }

    fun exists(key: String) = creatorsRegistry.containsKey(key)

    fun KClass<*>.asKey(): String = this.qualifiedName!!

}

class RegistryException(message: String) : Exception(message)

inline fun <reified T> singleton(noinline creator: Registry.() -> T) = Registry.DEFAULT.singleton(creator)

inline fun <reified T> normal(noinline creator: Registry.() -> T) = Registry.DEFAULT.normal(creator)

inline fun <reified T> inject() = Registry.DEFAULT.inject<T>()

